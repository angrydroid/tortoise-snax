import 'package:flutter/material.dart';

class SearchResultRouteArgs {
  SearchResultRouteArgs({this.title});
  final String title;
}