import 'package:flutter/material.dart';
import 'package:tortoise_snax/Model/plants.dart';

class PlantDetailsRouteArgs {
  PlantDetailsRouteArgs({this.plant});
  final Plants plant;
}