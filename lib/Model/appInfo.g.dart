// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appInfo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppInfo _$AppInfoFromJson(Map<String, dynamic> json) {
  return AppInfo(
      json['version'] as String,
      json['publishDate'] == null
          ? null
          : DateTime.parse(json['publishDate'] as String),
      json['description'] as String);
}

Map<String, dynamic> _$AppInfoToJson(AppInfo instance) => <String, dynamic>{
      'version': instance.version,
      'publishDate': instance.publishDate?.toIso8601String(),
      'description': instance.description
    };
