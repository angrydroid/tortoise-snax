import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import './edibles.dart';
import 'package:json_annotation/json_annotation.dart';
part 'plants.g.dart';

@JsonSerializable()
class Plants {
  String id;
  String rev;
  String type;
  String description;
  String leadImage;
  List<String> otherImages;
  Edibles edibles;
  String commonName;
  List<String> otherNames;
  String latinName;
  String imageUrl;
  int feedFrequency;
  String pageUrl;

  Plants(
    this.id,
    this.rev,
    this.type,
    this.description,
    this.leadImage,
    this.otherImages,
    this.edibles,
    this.commonName,
    this.otherNames,
    this.latinName,
    this.imageUrl,
    this.feedFrequency,
    this.pageUrl,
  );

  factory Plants.fromJson(Map<String, dynamic> json) => _$PlantsFromJson(json);
  Map<String, dynamic> toJson() => _$PlantsToJson(this);

  Plants.fromMap(Map<dynamic, dynamic> map) {
    if(map['type'] != null) {
      this.type = map['type'];
    }
    if(map['description'] != null) {
      this.description = map['description'];
    }
    if(map['leadImage'] != null) {
      this.leadImage = map['leadImage'];
    }
    if(map['otherImages'] != null) {
      this.otherImages = new List<String>.from(map['otherImages']); // cast to string list
    }
    if(map['edibles'] != null) {
      this.edibles = Edibles.fromMap(map['edibles']); // make edibles
    }
    if(map['commonName'] != null) {
      this.commonName = map['commonName'];
    }
    if(map['otherNames'] != null) {
      this.otherNames = new List<String>.from(map['otherNames']); // cast to string list
    }
    if(map['latinName'] != null) {
      this.latinName = map['latinName'];
    }
    if(map['imageUrl'] != null) {
      this.imageUrl = map['imageUrl'];
    }
    if(map['feedFrequency'] != null) {
      this.feedFrequency = map['feedFrequency'];
    }
    if(map['pageUrl'] != null){
      this.pageUrl = map['pageUrl'];
    }
  }

  UnmodifiableListView<String> get allImageUrls => UnmodifiableListView(new List.from(this.otherImages)..add(this.imageUrl));

}
