import 'package:json_annotation/json_annotation.dart';

part 'edibles.g.dart';

@JsonSerializable()

class Edibles {
  bool flowers;
  bool leaves;

  Edibles(this.flowers, this.leaves);

  factory Edibles.fromJson(Map<String, dynamic> json) => _$EdiblesFromJson(json);
  Map<String, dynamic> toJson() => _$EdiblesToJson(this);

  Edibles.fromMap(Map<dynamic, dynamic> map){
    if(map['flowers'] != null) {
      this.flowers = map['flowers'].toString().toLowerCase() == "true";
    }
    if(map['leaves'] != null) {
      this.leaves = map['leaves'].toString().toLowerCase() == "true";
    }
    }
}