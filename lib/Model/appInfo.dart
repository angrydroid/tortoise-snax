import 'package:json_annotation/json_annotation.dart';
part 'appInfo.g.dart';

@JsonSerializable()

class AppInfo {
  String version;
  DateTime publishDate;
  String description;

  AppInfo(
    this.version,
    this.publishDate,
    this.description
  );

  factory AppInfo.fromJson(Map<String, dynamic> json) => _$AppInfoFromJson(json);
  Map<String, dynamic> toJson() => _$AppInfoToJson(this);

  AppInfo.fromMap(Map<dynamic, dynamic> map) {
    if(map['version'] != null) {
      this.version = map['version'];
    }
    if(map['publishDate'] != null) {
      this.publishDate = DateTime.parse(map['publishDate']);
    }
    if(map['description'] != null) {
      this.description = map['description'];
    }
  }
}