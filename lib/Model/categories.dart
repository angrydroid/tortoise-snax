import 'package:json_annotation/json_annotation.dart';
part 'categories.g.dart';

@JsonSerializable()

class Category {
  String category;
  String imageUrl;
  String url;

  Category(
    this.category,
    this.imageUrl,
    this.url
  );

  factory Category.fromJson(Map<String, dynamic> json) => _$CategoryFromJson(json);
  Map<String, dynamic> toJson() => _$CategoryToJson(this);

  Category.fromMap(Map<dynamic, dynamic> map) {
    if(map['category'] != null) {
      this.category = map['category'];
    }
    if(map['imageUrl'] != null) {
      this.imageUrl = map['imageUrl'];
    }
    if(map['url'] != null) {
      this.url = map['url'];
    }
  }
}