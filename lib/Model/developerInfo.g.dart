// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'developerInfo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeveloperInfo _$DeveloperInfoFromJson(Map<String, dynamic> json) {
  return DeveloperInfo(json['name'] as String, json['email'] as String);
}

Map<String, dynamic> _$DeveloperInfoToJson(DeveloperInfo instance) =>
    <String, dynamic>{'name': instance.name, 'email': instance.email};
