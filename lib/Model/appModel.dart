import 'dart:collection';
import 'dart:math';
import './plants.dart';
import './categories.dart';
import 'package:scoped_model/scoped_model.dart';

class AppModel extends Model {
  // internal app state to provide names, plants and categories plus other importent states

  Map<String,Plants> _allPlants = Map();
  Map<String, String> _namesMap = Map();
  List<Category> _categories = List();

  // store to hold the search results.
  List<Plants> _searchResults = new List();

  // getters and setters to help set up the app state
  Map<String,Plants> get allPlants => _allPlants;
  set allPlants (value) {
    _allPlants = value;
    notifyListeners();
  }
  
  Map<String, String> get names => _namesMap;
  set names (value) {
    _namesMap = value;
    notifyListeners();
  }

  List<Category> get categories => _categories;
  set categories (value) {
    _categories = value;
    notifyListeners();
  }

  bool get loading {
    return !(_allPlants.length > 0) && !(_namesMap.length > 0) && !(_categories.length > 0);
  }

  // return the a list of plant search results that is imutable.
  UnmodifiableListView<Plants> get searchResults => UnmodifiableListView(_searchResults);

  // a function that gets call that allows the categories page to reuse search results for its own display.
  void setSeachResultsByCategory(String catName){
    _searchResults.clear();
    _allPlants.forEach((k,v) {
      if(v.type == catName)
      _searchResults.add(v);
    });
    // This call tells [Model] that it should rebuild the widgets that
    // depend on it.
    notifyListeners();
  }

  void setSeachResultsByNames (String name){
    String lowerName = name.toLowerCase();
    List<String> ids = new List();
    _searchResults.clear();
    _namesMap.forEach((k,v) {
      if(k.toLowerCase().contains(lowerName)) {
        ids.add(v);
      }
    });
    // remove duplicates:
    List<String> distinctIds = ids.toSet().toList();
    distinctIds.forEach((id) {
      _searchResults.add(_allPlants[id]);
    });
    // This call tells [Model] that it should rebuild the widgets that
    // depend on it.
    notifyListeners();
  }

  Plants getPlantById(String id){
    if(_allPlants.containsKey(id)){
      return _allPlants[id];
    } else {
      return null;
    }
    
  }

  Plants getRandomPlant() {
    List<Plants> plants =_allPlants.values.toList();
    var safePlants = plants.where((plant) => plant.feedFrequency >= 3 ).toList();
    Random rand = new Random();
    int value = rand.nextInt(safePlants.length);
    return safePlants[value];
  }

}