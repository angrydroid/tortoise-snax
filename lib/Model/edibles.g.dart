// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'edibles.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Edibles _$EdiblesFromJson(Map<String, dynamic> json) {
  return Edibles(json['flowers'] as bool, json['leaves'] as bool);
}

Map<String, dynamic> _$EdiblesToJson(Edibles instance) =>
    <String, dynamic>{'flowers': instance.flowers, 'leaves': instance.leaves};
