import 'package:json_annotation/json_annotation.dart';
part 'developerInfo.g.dart';

@JsonSerializable()

class DeveloperInfo {
  String name;
  String email;

  DeveloperInfo(
    this.name,
    this.email
  );

  factory DeveloperInfo.fromJson(Map<String, dynamic> json) => _$DeveloperInfoFromJson(json);
  Map<String, dynamic> toJson() => _$DeveloperInfoToJson(this);

  DeveloperInfo.fromMap(Map<dynamic, dynamic> map) {
    if(map['name'] != null) {
      this.name = map['name'];
    }
    if(map['email'] != null) {
      this.email = map['email'];
    }
  }
}