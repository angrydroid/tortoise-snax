// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'plants.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Plants _$PlantsFromJson(Map<String, dynamic> json) {
  return Plants(
      json['id'] as String,
      json['rev'] as String,
      json['type'] as String,
      json['description'] as String,
      json['leadImage'] as String,
      (json['otherImages'] as List)?.map((e) => e as String)?.toList(),
      json['edibles'] == null
          ? null
          : Edibles.fromJson(json['edibles'] as Map<String, dynamic>),
      json['commonName'] as String,
      (json['otherNames'] as List)?.map((e) => e as String)?.toList(),
      json['latinName'] as String,
      json['imageUrl'] as String,
      json['feedFrequency'] as int,
      json['pageUrl'] as String);
}

Map<String, dynamic> _$PlantsToJson(Plants instance) => <String, dynamic>{
      'id': instance.id,
      'rev': instance.rev,
      'type': instance.type,
      'description': instance.description,
      'leadImage': instance.leadImage,
      'otherImages': instance.otherImages,
      'edibles': instance.edibles,
      'commonName': instance.commonName,
      'otherNames': instance.otherNames,
      'latinName': instance.latinName,
      'imageUrl': instance.imageUrl,
      'feedFrequency': instance.feedFrequency,
      'pageUrl': instance.pageUrl
    };
