import 'package:flutter/material.dart';

const tsPrimary = const Color(0xFF33691E);
const tsPrimaryVariant = const Color(0xFF33691E);

const tsSafe = const Color(0xFF4CAF50);
const tsSafeVariant = const Color(0xFF388E3C);
const tsOccasionally = const Color(0xFFFFC107);
const tsOccasionallyVariant = const Color(0xFFA000);
const tsRarely = const Color(0xFFFF5722);
const tsRarelyVariant = const Color(0xFFE64A19);
const tsNever = const Color(0xFFF44336);
const tsNeverVariant = const Color(0xFFD32F2F);

const tsSurfaceWhite = Colors.white;
const tsBackground = const Color(0xFFC0CA33);