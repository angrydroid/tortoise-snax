import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../Model/appModel.dart';
import '../RouteArgs/searchResults.dart';

class SearchBar extends StatefulWidget implements PreferredSizeWidget {
  SearchBar({Key key, this.title}) : preferredSize = Size.fromHeight(56.0), super(key: key);

  final Widget title;

  @override
  final Size preferredSize; // default is 56.0

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  bool _openSearch  = false;
  String _searchText = "";
  Widget _appBarTitle = new Text("");
  Widget _orginalTitle = new Text("");
  Icon _searchIcon = new Icon(Icons.search);
  final TextEditingController _filter = new TextEditingController();

  _SearchBarState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  void _gotoSearchResults(AppModel app) {
    app.setSeachResultsByNames(_searchText);
    Navigator.pushNamed(
      context, 
      '/searchResults', 
      arguments: SearchResultRouteArgs(
        title: _searchText
      )
    );
  }

  void _searchIconPressed(AppModel app) {
    setState(() {
      if (!this._openSearch) {
        this._openSearch = true;
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          decoration: new InputDecoration(
            prefixIcon: new Icon(Icons.search),
            hintText: 'Search...',
          ),
          autofocus: true,
          textInputAction: TextInputAction.done,
          onSubmitted: (string) {
            _gotoSearchResults(app);
          },
        );
      } else {
        this._openSearch = false;
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = _orginalTitle;
        _filter.clear();
      }
    });
  }

  List<Widget> _buildActions (bool searchActive) {
    List<Widget> actions = new List<Widget>();

    if(searchActive){
      actions.add(
        ScopedModelDescendant<AppModel>(
          builder: (context, child, app) {
            return IconButton(
              icon: Icon(Icons.arrow_forward),
              onPressed: () {
                _gotoSearchResults(app);
              }
            );
          }
        )
      );
    }

    return actions;
  }

  @override
  Widget build(BuildContext context) {
    _orginalTitle = widget.title;
    if(!_openSearch){
      _appBarTitle = _orginalTitle;
    }
    return AppBar(
      title: _appBarTitle, 
      leading: ScopedModelDescendant<AppModel>(
        builder:(context, child, app) {
          return IconButton(
            icon: _searchIcon,
            onPressed: () {
              _searchIconPressed(app);
            },
          );
        }
      ),
      actions: _buildActions(_openSearch),
    );
  }

}