import 'package:flutter/material.dart';
import 'package:tortoise_snax/icons.dart';

class FeedChip extends StatelessWidget {
  FeedChip({Key key, this.frequencey, this.leaves, this.flowers, this.small =false}) : super (key: key);
  final int frequencey;
  final bool leaves;
  final bool flowers;
  final bool small;

  Color _colourBackgroundSelector(int feedFreq){
    switch (feedFreq) {
      case 0:
        return Colors.red[700];
      case 1:
        return Colors.deepOrange[700];
      case 2:
        return Colors.amber[700];
      case 3:
        return Colors.green[700];
      default:
       return Colors.white;
    }
  }

  Color _colourAccentSelector(int feedFreq){
    switch (feedFreq) {
      case 0:
        return Colors.red[500];
      case 1:
        return Colors.deepOrange[500];
      case 2:
        return Colors.amber[500];
      case 3:
        return Colors.green[500];
      default:
       return Colors.white;
    }
  }

  String _textSelector(int feedFreq){
    switch (feedFreq) {
      case 0:
        return "Do Not Feed!";
      case 1:
        return "Feed Rarely";
      case 2:
        return "Feed Occasionally";
      case 3:
        return "Safe To Feed";
      default:
       return "Unknown, Do not feed";
    }
  }

  Icon _iconSelector(int feedFreq) {
    switch(feedFreq) {
      case 0:
        return Icon(
          Icons.close, 
          color: Colors.white,
        );
      case 1:
        return Icon(
          Icons.priority_high, 
          color: Colors.white,
        );
      case 2:
        return Icon(
          Icons.access_time, 
          color: Colors.white,
        );
      case 3:
        return Icon(
          Icons.check, 
          color: Colors.white,
        );
      default:
        return Icon(
          Icons.close, 
          color: Colors.white,
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: small? 50 :75,
      child: Center(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(new Radius.circular(50)),
                  color: _colourBackgroundSelector(frequencey),
                ),
                constraints: BoxConstraints.expand(),
              ),
              Positioned(
                left: 60,
                bottom: small? 5:17,
                child: Container(
                  child: Text(
                    _textSelector(frequencey),
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Colors.white,
                    ),
                  ),                
                ),
              ),
              Positioned(
                left: small? 0.5: 2.5,
                height: small? 25:50,
                width: 50,
                bottom: 2.5,
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _colourAccentSelector(frequencey)
                  ),
                  child: Center(
                    child: _iconSelector(frequencey),
                  ),
                ),
              ),
              Positioned(
                height: small? 25:50,
                width: 100,
                bottom: 2.5,
                right: 2.5,
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle, 
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(50)
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Icon(
                        CustomIcons.flower_poppy,
                        color: flowers ? Colors.lightGreenAccent[700] : Colors.redAccent[700],
                      ),
                      Icon(
                        CustomIcons.leaf,
                        color: leaves ? Colors.lightGreenAccent[700] : Colors.redAccent[700],
                      ),
                    ],
                  )
                ),
              )
            ],
          )
        )
      )
    );
  }
}