import 'dart:async';
import 'dart:convert';
import 'dart:io' show Platform;

import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tortoise_snax/Pages/search.dart';
import 'colors.dart';
import 'Pages/home.dart';
import 'Pages/about.dart';
import 'Pages/searchResults.dart';
import 'Pages/categories.dart';
import 'Pages/plantDetails.dart';

import './Model/appModel.dart';
import './Model/plants.dart';
import './Model/categories.dart';
import './Model/appInfo.dart';
import './Model/developerInfo.dart';

Future<void> main() async {
  final FirebaseApp app = await FirebaseApp.configure(
    name: 'db2',
    options: Platform.isIOS
        ? const FirebaseOptions(
            googleAppID: '1:469713748712:ios:73813397f118dbca', // TODO: add iso firebase config support
            gcmSenderID: '469713748712',
            databaseURL: 'https://tortoise-eddiebles.firebaseio.com',
          )
        : const FirebaseOptions(
            googleAppID: '1:469713748712:android:08d166540d64f8fa',
            apiKey: 'AIzaSyBUW7XQ12IOx5-AddKViCnalJpcnV1RehI',
            databaseURL: 'https://tortoise-eddiebles.firebaseio.com',
          ),
  );

  // init the app state model
  final AppModel appModel = AppModel();
  
  // run the app using scopedmodel to bind the app model
  runApp(
    ScopedModel<AppModel>(
      model: appModel,
      child: App(),
    )
  );
}

final ThemeData _tsTheme = _buildTSTheme();

ThemeData _buildTSTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    accentColor: tsPrimaryVariant,
    primaryColor: tsPrimary,
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: tsPrimaryVariant,
      textTheme: ButtonTextTheme.normal,
    ),
    scaffoldBackgroundColor: tsBackground,
    cardColor: tsSurfaceWhite,
    textSelectionColor: tsPrimary,
    errorColor: tsNever,
    // TODO: Add the text themes (103)
    // TODO: Add the icon themes (103)
    // TODO: Decorate the inputs (103)
  );
}

class _AppState extends State<App> {
  DatabaseReference _plantsRef;
  DatabaseReference _categoriesRef;
  DatabaseReference _namesRef;

  String disclaimer;
  String sourceAcknowlement;
  AppInfo appInfo;
  DeveloperInfo devInfo;

  @override
  void initState(){
    super.initState();
    // Demonstrates configuring to the database using a file
    _plantsRef = FirebaseDatabase.instance.reference().child('plants');
    _categoriesRef = FirebaseDatabase.instance.reference().child('categories');
    _namesRef = FirebaseDatabase.instance.reference().child('names');

    _plantsRef.once().then((DataSnapshot snapshot){
      // take the plant data and store internally
      Map<String,Plants> _allPlants = Map();
      Map<dynamic, dynamic> rawPlants = snapshot.value;
      rawPlants.forEach((k,v) {
        _allPlants[k] = Plants.fromMap(v);
      });
      ScopedModel.of<AppModel>(context, rebuildOnChange: false).allPlants =_allPlants;
    });

    _categoriesRef.once().then((DataSnapshot snapshot) {
      List<Category> _categories = List();
      Map<dynamic, dynamic> rawCategories = snapshot.value;
      rawCategories.forEach((k,v) {
        _categories.add(Category.fromMap(v));
      });
      ScopedModel.of<AppModel>(context, rebuildOnChange: false).categories =_categories;
    });

    _namesRef.once().then((DataSnapshot snapshot) {
      Map<String, String> _namesMap = Map();
      Map<dynamic, dynamic> rawName = snapshot.value;
      rawName.forEach((k,v) {
        //v is a list of object that need to be unpacked and rearraged
        v.forEach((nameObj) {
          _namesMap[nameObj['name'].toString()] = nameObj['id'].toString();
        });
      });
      ScopedModel.of<AppModel>(context, rebuildOnChange: false).names =_namesMap;
    });

    _loadDisclaimer().then((value) {
      this.disclaimer = value;
    });

    _loadSourceAcknowlegement().then((value) {
      this.sourceAcknowlement = value;
    });

    _loadAppInfo().then((value) {
      Map<String, dynamic> appdata = jsonDecode(value);
      this.appInfo = AppInfo.fromJson(appdata);
    });

    _loadDevInfo().then((value) {
      Map<String, dynamic> devdata = jsonDecode(value);
      this.devInfo = DeveloperInfo.fromJson(devdata);
    });

  } 

  Future<String> _loadDisclaimer() async {
    return await rootBundle.loadString('assets/text/disclaimer.txt');
  }

  Future<String> _loadSourceAcknowlegement() async {
    return await rootBundle.loadString('assets/text/orginalSourceAcknowlegment.txt');
  }

  Future<String> _loadAppInfo() async {
    return await rootBundle.loadString('assets/json/appInfo.json');
  }

  Future<String> _loadDevInfo() async {
    return await rootBundle.loadString('assets/json/devInfo.json');
  }

  @override
  Widget build(BuildContext context) {
    

    return MaterialApp(
      title: 'Tortise Snax',
      theme: _tsTheme,
      initialRoute: '/',
      routes: {
        '/': (BuildContext context) => HomePage(title: 'Tortoise Snax'),
        '/about': (BuildContext context) => AboutPage(
          disclaimerText: disclaimer,
          orginalSourceText: sourceAcknowlement,
          appInfo: appInfo,
          devInfo: devInfo,
        ),
        '/searchResults': (BuildContext context) => SearchResultsPage(title: 'Search Results',),
        '/search': (BuildContext context) => SearchPage(),
        '/categories': (BuildContext context) => CategoriesPage(),
        '/plantDetails': (BuildContext context) => PlantDetailsPage(),
      },
    );
  }

}

class App extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _AppState createState() => _AppState();
}


