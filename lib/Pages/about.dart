import 'package:flutter/material.dart';
import '../Model/appInfo.dart';
import '../Model/developerInfo.dart';

class AboutPage extends StatelessWidget {
  AboutPage({Key key, this.disclaimerText, this.orginalSourceText, this.appInfo, this.devInfo}) :super (key: key);

  final String disclaimerText;
  final String orginalSourceText;
  final AppInfo appInfo;
  final DeveloperInfo devInfo;

  Widget _disclaimerCard(BuildContext context, String disclaimer) {
    return Card(
      child: Padding(
        padding:EdgeInsets.all(5),
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(5),
                child: Text(
                  "Discalimer",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Text(disclaimer),
            ],
          ),
        ),
      )
    );
  }

  Widget _orginalSourceCard(BuildContext context, String orginalSource) {
    return Card(
      child: Padding(
        padding:EdgeInsets.all(5),
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(5),
                child: Text(
                  "Data Source Acknowlegment",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Text(orginalSource),
            ],
          ),
        ),
      )
    );
  }

  Widget _appInfoCard(BuildContext context, AppInfo info) {
    return Card(
      child: Padding(
        padding:EdgeInsets.all(5),
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(5),
                child: Text(
                  "Application Information",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Version: ",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 10,
                      fontWeight: FontWeight.w400,
                      color: Colors.grey
                    ),
                  ),
                  Text(
                    info.version,
                    textAlign: TextAlign.right,
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Publish Date: ",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 10,
                      fontWeight: FontWeight.w400,
                      color: Colors.grey
                    ),
                  ),
                  Text(
                    "${info.publishDate.day}/${info.publishDate.month}/${info.publishDate.year}",
                    textAlign: TextAlign.right,
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Description: ",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 10,
                      fontWeight: FontWeight.w400,
                      color: Colors.grey
                    ),
                  ),
                  Expanded(
                    child: Text(
                      info.description,
                      textAlign: TextAlign.right,
                      softWrap: true,
                    )
                  )
                ],
              ),
            ],
          ),
        ),
      )
    );
  }

  Widget _devInfoCard(BuildContext context, DeveloperInfo info) {
    return Card(
      child: Padding(
        padding:EdgeInsets.all(5),
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(5),
                child: Text(
                  "Developer Information",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Name: ",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 10,
                      fontWeight: FontWeight.w400,
                      color: Colors.grey
                    ),
                  ),
                  Text(
                    info.name,
                    textAlign: TextAlign.right,
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Email: ",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 10,
                      fontWeight: FontWeight.w400,
                      color: Colors.grey
                    ),
                  ),
                  Text(
                    info.email,
                    textAlign: TextAlign.right,
                  )
                ],
              ),
            ],
          ),
        ),
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About'),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _disclaimerCard(context, disclaimerText),
              _orginalSourceCard(context, orginalSourceText),
              _appInfoCard(context, appInfo),
              _devInfoCard(context, devInfo)
            ],
          ),
        ),
      ),
    );
  }
}