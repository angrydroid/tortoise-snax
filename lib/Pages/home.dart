import 'package:flutter/material.dart';
import 'package:tortoise_snax/RouteArgs/plantDetails.dart';
import 'package:tortoise_snax/Widgets/feedChip.dart';
import 'package:tortoise_snax/colors.dart';
import 'package:scoped_model/scoped_model.dart';
import '../Widgets/searchBar.dart';
import '../Model/appModel.dart';
import '../Model/plants.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  void _onItemTapped(int index) {
    switch (index) {
      case 0:
          Navigator.pushNamed(context, '/search');
        break;
      case 1:
          Navigator.pushNamed(context, '/categories');
        break;
      case 2:
          Navigator.pushNamed(context, '/about');
        break;
      default:
        Navigator.pushNamed(context, '/');
    }
  }

  Widget _loadingData() {
    return Padding(
      padding: EdgeInsets.all(15),
      child: Column(
        children: <Widget>[
          new CircularProgressIndicator(),
          Padding(
            padding: EdgeInsets.all(5),
            child: new Text("Loading"),
          )
      ],),
    );
  }

  Widget _otherNamesString(Plants plant){
   if(plant.otherNames == null){
     return Text("");
   } else {
     return Text(
        plant.otherNames.join(', '),
        textAlign: TextAlign.left,
        style: TextStyle(
          fontSize: 12,
          fontStyle: FontStyle.normal,
        ),
      );
   }
 }

  Widget _buildCard(Plants plant){
    return  Column(
      children: <Widget>[
        Image.network(
          plant.imageUrl,
          fit:BoxFit.cover,
          width: MediaQuery.of(context).size.width-10,
        ),
        Text(
          plant.commonName,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold
          ),
        ),
        Text(
          plant.latinName,
          style: TextStyle(
            fontSize: 15,
            fontStyle: FontStyle.italic,
            fontWeight: FontWeight.w300,
            color: Colors.grey
          ),
        ),
        _otherNamesString(plant),
        FeedChip(
          frequencey: plant.feedFrequency,
          leaves: plant.edibles != null && plant.edibles.leaves != null ? plant.edibles.leaves: false,
          flowers: plant.edibles != null && plant.edibles.flowers != null ? plant.edibles.flowers: false,
          small: true,
        ),
        FlatButton(
          child: Text("Details"),
          textColor: Colors.lightGreen[800],
          onPressed: () {
            Navigator.pushNamed(
              context,
              '/plantDetails',
              arguments: PlantDetailsRouteArgs(plant: plant)
            );
          },
        )
      ],
    );
  }

  Widget _randomPlantCard() {
    return Card(
      child: Container(
        width: MediaQuery.of(context).size.width-10,
        child: ScopedModelDescendant<AppModel>(
          builder: (context, child, app) {
            if(app.loading){
              return _loadingData();
            } else {
              Plants randPlant = app.getRandomPlant();
              return _buildCard(randPlant);
            }
          }
        )
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: SearchBar(
        title: Text(widget.title),
      ),
      body: Center(
        child:SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget> [
              Text(
                "Discover New Snax",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight:FontWeight.bold,
                ),
              ),
              _randomPlantCard()
            ],
          ),
        ),
      ),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          // sets the background color of the `BottomNavigationBar`
          canvasColor: tsPrimary
        ),
        child: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                Icons.search, 
                color: Colors.white,
              ), 
              title: Text(
                'Search', 
                style: TextStyle(
                  color: Colors.white,
                ),
              )
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.category, 
                color: Colors.white,
              ), 
              title: Text(
                'Categories', 
                style: TextStyle(color: Colors.white,),
              )
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.info, 
                color: Colors.white,
              ), 
              title: Text(
                'About',
                 style: TextStyle(color: Colors.white,),
              )
            ),
          ],
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}