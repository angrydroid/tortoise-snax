import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter_image/network.dart';
import '../Model/categories.dart';
import '../Model/appModel.dart';
import '../RouteArgs/searchResults.dart';

class CategoriesPage extends StatelessWidget {

  Widget _categoiesGrid(){
    return ScopedModelDescendant<AppModel>(
      builder: (context, child, app) {
        List<Category> categories = app.categories;
        return GridView.builder(
          itemCount: categories.length,
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
          itemBuilder: (BuildContext context, int index) {
            return new Card(
              semanticContainer: true,
              child: Material(
                color: Colors.amber[200],
                child: InkWell(
                  splashColor: Colors.amber,
                  onTap: () {
                    app.setSeachResultsByCategory(categories[index].category);
                    Navigator.pushNamed(
                      context, 
                      '/searchResults', 
                      arguments: SearchResultRouteArgs(
                        title: categories[index].category
                      )
                    );
                  },
                  child: new Column(
                    children: <Widget>[
                      Image(
                      image: new NetworkImageWithRetry(categories[index].imageUrl),
                      ),
                      Padding(
                        padding: EdgeInsets.all(4), 
                        child: Text(
                          categories[index].category,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ], 
                  ),
                ),
              ),
            );
          },
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Categories'),
      ),
      body: Center(
        child: _categoiesGrid()
      ),
    );
  }
}