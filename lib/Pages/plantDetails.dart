import 'package:flutter/material.dart';
import 'package:tortoise_snax/RouteArgs/plantDetails.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../Model/plants.dart';
import '../Widgets/feedChip.dart';

class PlantDetailsPage extends StatefulWidget{
  PlantDetailsPage({Key key, this.plant}) :super (key: key);

  final Plants plant;

  @override
  _PlantDetailsState createState() => _PlantDetailsState();
}
  
class _PlantDetailsState extends State<PlantDetailsPage>{
  Widget _otherNamesString(Plants plant){
   if(plant.otherNames == null){
     return Text("");
   } else {
     return Text(
        plant.otherNames.join(', '),
        textAlign: TextAlign.left,
        style: TextStyle(
          fontSize: 12,
          fontStyle: FontStyle.normal,
        ),
      );
   }
 }

  @override
  Widget build(BuildContext context) {
    final PlantDetailsRouteArgs args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text(args.plant.commonName),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CarouselSlider(
                viewportFraction: 0.9,
                aspectRatio: 2.0,
                autoPlay: true,
                enlargeCenterPage: true,
                pauseAutoPlayOnTouch: Duration(seconds: 3),
                height: 250.0,
                items: args.plant.allImageUrls.map((i) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                        margin: EdgeInsets.all(5.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          child: Image.network(
                            i,
                            fit: BoxFit.cover,
                            width: 1000.0,
                          ),
                        ),
                      );
                    },
                  );
                }).toList(),
              ),
              FeedChip(
                frequencey: args.plant.feedFrequency,
                leaves: args.plant.edibles != null && args.plant.edibles.leaves != null ? args.plant.edibles.leaves : false,
                flowers: args.plant.edibles != null && args.plant.edibles.flowers != null ? args.plant.edibles.flowers : false,
              ),
              Card(
                child: Column(
                  children: <Widget>[
                    Text(
                      args.plant.commonName,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 20,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      args.plant.latinName,
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontSize: 15,
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.w300,
                        color: Colors.grey,
                      ),
                    ),
                    _otherNamesString(args.plant),
                    Padding(
                      padding: EdgeInsets.all(10), 
                      child: Text(args.plant.description),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
