import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tortoise_snax/Model/appModel.dart';
import 'package:tortoise_snax/RouteArgs/plantDetails.dart';
import 'package:tortoise_snax/RouteArgs/searchResults.dart';
import '../Model/appInfo.dart';
import '../Model/developerInfo.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  Map<String, String> _nameShortList = new Map();
  Map<String, String> _allNames;
  TextEditingController _filter = TextEditingController();
  String _searchTerm = "";
  int _shortListLimit = 30;

  @override
  void initState() {
    _allNames = ScopedModel.of<AppModel>(context).names;
    _setupShorList(_shortListLimit);
  }

  void _setupShorList(int limit){
    int count = 0;
    _allNames.forEach((k,v) {
      if(count > limit){
        return;
      }
      _nameShortList[k] = v;
      count++;
    });
  }

  Map<String, String> _reduceMap(Map<String, String> map, int limit){
    int count = 0;
    Map<String,String> dummyMap = Map();
    map.forEach((k,v) {
      if(count > limit){
        return;
      }
      dummyMap[k] = v;
      count++;
    });
    return dummyMap;
  }

  void _filterNames(String query){
    if(query.isNotEmpty){
      Map<String,String> dummyMap = new Map();
      _allNames.forEach((k,v) {
        if(k.toLowerCase().contains(query.toLowerCase())){
          dummyMap[k] = v;
        }        
      });
      setState(() {
        _nameShortList.clear();
        if(dummyMap.length > _shortListLimit){
          dummyMap =_reduceMap(dummyMap, _shortListLimit);
        }
        _nameShortList.addAll(dummyMap);
      });
    } else {
      setState(() {
        _nameShortList.clear();
        _setupShorList(_shortListLimit);
      });
    } 
  }

  void _gotoSearchResults(AppModel app, [String value]){
    if(value != null){
      app.setSeachResultsByNames(_searchTerm);
    } else {
      app.setSeachResultsByNames(_searchTerm);
    }
    Navigator.pushNamed(
      context, 
      '/searchResults', 
      arguments: SearchResultRouteArgs(
        title: _searchTerm
      )
    );
  }

  List<Widget> _nameTiles(BuildContext context) {
    List<String> keys =  _nameShortList.keys.toList();
    List<Card> nameCards = new List<Card>.generate(keys.length, (int index) {
      return Card(
        child: ScopedModelDescendant<AppModel>(
          builder: (context, child, app) {
            return GestureDetector(
              child: Container( 
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Text(keys[index]),
                ),
              ),
              onTap: () {
                Navigator.pushNamed(
                  context,
                  '/plantDetails',
                  arguments: PlantDetailsRouteArgs(plant: app.getPlantById(_nameShortList[keys[index]]))
                );
              },
            );
          }
        )
      );
    });
    return nameCards;
  }

  Widget _searchBox(BuildContext context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(5),
        child: ScopedModelDescendant<AppModel>(
          builder: (context, child, app) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width*0.75,
                  child: TextField(
                    autocorrect: true,
                    decoration: InputDecoration(
                      hintText: "Search plants"
                    ),
                    onChanged: (value) {
                      _searchTerm = value;
                      _filterNames(value);
                    },
                    controller: _filter,
                    onSubmitted: (value) {
                      _gotoSearchResults(app);
                    },
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    _gotoSearchResults(app);
                  },
                )
              ],
            );
          }
        ),
      ), 
    );
  }

  List<Widget> _buildColumnChildren(BuildContext context) {
    List<Widget>nameCards =_nameTiles(context);
    Widget search =_searchBox(context);
    nameCards.insert(0, search);
    return nameCards;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Search'),
      ),
      body: Center(    
        child:SingleChildScrollView(
          child:Column(children: _buildColumnChildren(context),)
        ),
      ),
    );
  }
}