import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tortoise_snax/RouteArgs/plantDetails.dart';
import 'package:tortoise_snax/RouteArgs/searchResults.dart';
import '../Model/plants.dart';
import '../Model/appModel.dart';

class SearchResultsPage extends StatelessWidget {
  SearchResultsPage({Key key, this.title}) : super(key: key);

  final String title;

  Widget _resultCardsView(){
    return ScopedModelDescendant<AppModel>(
      builder: (context, child, app) {
        var results = app.searchResults;
        if(results.length > 0){
          return GridView.extent(
            maxCrossAxisExtent: 250.0,
            mainAxisSpacing: 5.0,
            crossAxisSpacing: 5.0,
            padding: const EdgeInsets.all(5.0),
            children: _buildResultCards(context, results),
          );
        } else {
          return Container(
            child: Center(
              child: new Text('No Results Found.'),
            ),
          );
        }
        
      }
    );
  }
  

  List<Widget> _buildResultCards(BuildContext context, List<Plants> results) {
    List<GestureDetector> containers = new List<GestureDetector>.generate(results.length, (int index) {
      return GestureDetector(
        child: Stack(
          children: <Widget>[
            Container(
              color: Colors.white,
            ),
            Container(
              child: Image.network(
                results[index].imageUrl,
                width: 250,
                height: 250,
                fit: BoxFit.cover,
              ),
            ),
            Positioned(
              child: Opacity(
                opacity: 0.70,
                child: Container(
                  color: _colourSelector(results[index].feedFrequency),
                ),
              ), 
              width: 250,
              height: 50,
              bottom: 0,
            ),
            Positioned(
              child: Text(
                results[index].commonName, 
                overflow:TextOverflow.fade, 
                style: TextStyle(
                  fontWeight: FontWeight.bold, 
                  fontSize: 20, 
                  color: Colors.white
                ),
              ),
              width: 150,
              height: 50,
              left: 2,
              bottom: 0,
            ),
          ],
        ),
        onTap: () {
          Navigator.pushNamed(
            context,
            '/plantDetails',
            arguments: PlantDetailsRouteArgs(plant: results[index])
          );
        },
      );
    });

    return containers;
  }

  Color _colourSelector(int feedFreq){
    switch (feedFreq) {
      case 0:
        return Colors.red[700];
      case 1:
        return Colors.deepOrange[700];
      case 2:
        return Colors.amber[700];
      case 3:
        return Colors.green[700];
      default:
       return Colors.white;
    }
  }

  @override
  Widget build(BuildContext context) {

    final SearchResultRouteArgs args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text(args.title),
      ),
      body: Center(
        child: _resultCardsView(),
      ),
    );
  }
}